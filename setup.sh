#!/bin/bash
# neovim setup
#
# [Fetch git repo]? then
# [compile]? then
# [install locally]
#
# martinx (27/07/2023)

### begin parameters
#	WARNING: unsafe, parameters not sanitized.
INSTALL_PREFIX=${INSTALL_PREFIX:-"${HOME}/local/install/tool"}
DIR_SRC=${DIR_SRC:-"${HOME}/local/compile/tool/neovim"}
FORCE_CLEAN=${FORCE_CLEAN:-""}
VERBOSE=${VERBOSE:-""}

GIT_REPO=${GIT_REPO:-"https://github.com/neovim/neovim"}
GIT_CMD=${GIT_CMD:-"git clone -b nightly --filter=tree:0"}

STATUS_HEADER=${STATUS_HEADER:-'=== neovim setup ==='}
### end parameters

set -o nounset
set -e

MAKE_VERBOSITY="--quiet"
if [[ -n ${VERBOSE} ]]; then
	MAKE_VERBOSITY=""
fi

status() {
	echo "$STATUS_HEADER" "$@"
}

tryClone() {
	status "Checking local disk for existing repository..."
	if [[ ! -d "${DIR_SRC}/.git" ]]; then
		mkdir -p "$DIR_SRC"
		$GIT_CMD $GIT_REPO "$DIR_SRC"
	fi
	[[ -d "${DIR_SRC}/.git" ]] || {
		status "ASSERT FAIL: could not find git repo";
		exit 1;
	}
	status "OK: local repository available"
}

tryCompile() {
	status "Compiling..."
	local saveDir="$PWD"
	cd "${DIR_SRC}"
	if [[ -n ${FORCE_CLEAN} ]]; then
		make clean
		rm -rf build
		rm -rf .deps
	fi
	make CMAKE_BUILD_TYPE=RelWithDebInfo \
	     CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=\"${INSTALL_PREFIX}\"" \
             ${MAKE_VERBOSITY} && true
	if (( $? == 0 )); then
		status "OK: Compilation success"
	else
		status "ERROR: Compilation failed"
		status "   recommended: set FORCE_CLEAN when calling this script"
		exit 1
	fi
	cd "$saveDir"
}

tryInstall() {
	status "Installing..."
	# sudo only when necessary, good for non-interactive batchmode
	local saveDir="$PWD"
	cd "${DIR_SRC}"
	local installCommand="make install ${MAKE_VERBOSITY}"

	# the write test would always fail for empty dir,
	# hence we try to create the install prefix first
	mkdir -p ${INSTALL_PREFIX} 2>/dev/null
	if [[ ! -w ${INSTALL_PREFIX} ]]; then
		status "INFO: need root/sudo to write in ${INSTALL_PREFIX}"
		# no write permission, sudo required
		installCommand="sudo ${installCommand}"
	fi
	$installCommand

	cd "$saveDir"
	status "OK: Install"
	status "NOTE: export PATH=${INSTALL_PREFIX}/bin:\${PATH}"
}

stowLocalConfig() {
	SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
	status "Detected script location: ${SCRIPT_DIR}"
	status "Applying stow package..."

	mkdir -p "${HOME}/.config"
	stow -v stow_package/ --dir="${SCRIPT_DIR}" --target="${HOME}"
	if (( $? != 0 )); then
		status "ERROR: stow failed"
		exit 1
	else
		status "OK: stow package applied"
	fi
}

tryClone
tryCompile
tryInstall
stowLocalConfig
