-- map leader to <Space>
-- 	NOTE: must remap before loading plugins
vim.keymap.set("n", " ", "<Nop>", { silent = true, remap = false })
vim.g.mapleader = " "

require("pluginSetup")


-- ====================
-- === nvim options ===
-- ====================
-- Allow buffers to be hidden without losing information
vim.opt.hidden = true

-- disable mouse integration
vim.opt.mouse = ""

-- tweak left-most column
--	* Show line numbers
vim.opt.number = true
vim.opt.relativenumber = true
--	* Diagnostics / icon column
vim.opt.signcolumn = "yes"

-- Indentation rules and feedback
-- 	'list' shows white-space
vim.opt.list = true
vim.opt.listchars = "tab:»-,trail:·,extends:»,precedes:«"
vim.opt.expandtab = true
vim.opt.softtabstop = 8
vim.opt.tabstop = 8
vim.opt.shiftwidth = 8

-- Editor ergonomics
-- 80-column target
vim.opt.colorcolumn=vim.fn.join(vim.fn.range(81,81),",")
-- vim.cmd([[
--      :hi! link ColorColumn Normal
--      :hi ColorColumn cterm=bold
-- ]])

-- highlight current line (linked to ColorColumn)
vim.opt.cursorline = true

-- Functions
vim.cmd([[
" command: <Leader>nani
"   Identify highlight colors under cursor
"
nnoremap <Leader>nani :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") ."> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
]])

-- Terminal binding: switch mode to Normal with Esc
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>')
-- :tnoremap <Esc> <C-\><C-n> 

-- Telescope bindings
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
