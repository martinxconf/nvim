
-- === BOOTSTRAP PACKAGE MANAGER (lazy.nvim) ===
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    --[[
        COLORSCHEMES
    ]]--
    {
        "rebelot/kanagawa.nvim", name = "kanagawa", lazy = false, priority = 1000,
        config = function() require("themeConfig.kanagawa") end
    },
    {
        "folke/tokyonight.nvim", name = "tokyonight", lazy = false, priority = 1000,
        config = function() require("themeConfig.tokyonight") end
    },
    { "bluz71/vim-nightfly-colors", name = "nightfly", lazy = false },
    { "savq/melange-nvim", name = "melange", lazy = false },
    { "jacoborus/tender.vim", lazy = false },

    --[[
        TWEAK DISPLAY
    ]]--
    {
        "nvim-lualine/lualine.nvim",
        name = "lualine",
        lazy = false,
        config = function()
            require("pluginConfig.lualine")
        end
    },
    "https://github.com/psliwka/vim-smoothie",

    --[[
        TEXT MANIPULATION
    ]]--
    -- comment/uncomment
    --  Usage: MAPS 'gc', 'gcc'
         -- :7,17Commentary
         -- :g/TODO/Commentary
    "tpope/vim-commentary",
    "tpope/vim-fugitive",
    "airblade/vim-gitgutter",
    "chaoren/vim-wordmotion",

    --[[
    --  FILE NAVIGATION
    ]]--
    {
    'nvim-telescope/telescope.nvim', tag = '0.1.2',
-- or                              , branch = '0.1.x',
      dependencies = { 'nvim-lua/plenary.nvim' }
    },

    --[[
        LSP (Language Server Protocol)
    ]]--
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            "simrat39/rust-tools.nvim",
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
        },
        config = function()
            require("lspConfig/masonStack")
        end
    },

})

-- Set colorscheme
vim.cmd("colorscheme tender")
-- vim.cmd("colorscheme tokyonight-night")
-- vim.cmd("colorscheme kanagawa")
